const http = require('http')
const port = 3000;
const memeye = require('memeye');
memeye();

var c=0;
const requestHandler = (request, response) => {
  console.log(++c + "-rx")
  setTimeout(function() {
        response.end('done ');
  }, 5000);
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
})